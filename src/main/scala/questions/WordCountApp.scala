package questions

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

/**
  30%
  請使用sc.textFile讀取路徑dataset/hello.txt，並計算WordCount
  輸出結果為
  world,2
  Hello,2
  are,1
  hello,1
  how,1
  you,1

  */

object WordCountApp {
  val conf = new SparkConf().setAppName("WordCount")
    .setMaster("local[*]")
  val sc = new SparkContext(conf)


  val lines: RDD[String] =sc.textFile("dataset/hello.txt")
  val words: RDD[String] =lines.flatMap(str=>str->1)

  val wordCount:RDD[(String,Int)]=words().reduceByKey()


  wordCount.foreach(println)

//老師抱歉我不是沒讀 是背了就忘 下次會學習用理解的 可以再給分數低的同學最後一次機會嗎？ 期末考加分題可以多一點嗎？ 我會盡我所能去試試看的
//我真的很希望能通過這科  謝謝老師!


}
